import { useState } from "react";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Alert from "@mui/material/Alert";


function FindFactorial(number) {
    if (number === 1) {
        return 1
    } else {
        return number * FindFactorial(number - 1);
    }
}

export function FactorialForm() {
    const [factorialNumber, setFactorialNumber] = useState(0);
    const [error, setError] = useState('');
    const [num, setNum] = useState();

    const numberChanged = (e) => {
        e.preventDefault();
        setNum(e.target.value);
    }

    const buttonClicked = () => {
        if (num === '' || typeof num === 'undefined') {
            setError("Введіть номер!");
            return
        } else {
            if (!isNaN(num)) {
                setNum(parseInt(num));

                if (0 < num && num < 21) {
                    setError(null);
                    const factorial = FindFactorial(num);
                    setFactorialNumber(factorial);
                } else {
                    setError("Номер не є в проміжку 0 < номер < 21!");
                }
            } else {
                setError("Це не номер!");
            }
        }
    }

    const closeAlert = () => {
        setError(null);
    }


    return (
        <Box sx={{ width: 450, p:5 }}>
            <Stack spacing={{ xs: 1, sm: 2 }} direction="row" useFlexGap flexWrap="wrap">
                <TextField sx={{ width: 250 }} onChange={numberChanged} component={Paper} placeholder="Введіть номер" />
                <Button variant="contained" onClick={buttonClicked}>Знайти факторіал</Button>
                { error ? <Alert onClose={closeAlert} severity="error" sx={{ width: "100%" }}>{error}</Alert> : null }
                { factorialNumber ? 
                <Typography color='white' align="center" variant="h5" sx={{ width: '100%' }}>Результат: {factorialNumber}</Typography> : null
                }
            </Stack>
        </Box>
    )
}