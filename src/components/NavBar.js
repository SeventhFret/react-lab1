import { useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { orange, green, red, blue } from '@mui/material/colors';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import TitleIcon from '@mui/icons-material/Title';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';



export default function CreateNavBar({ color = 'orange' }) {
  const [headerToggle, setHeaderToggle] = useState(true);
  const reactColors = {
    'green': green[500],
    'orange': orange[500],
    'red': red[500],
    'blue': blue[500]
  }
  
  
  const navTheme = createTheme({
      palette: {
          primary: {
              main: reactColors[color],
          },    
      },
  })

  const updateHeader = () => {
    if (headerToggle) {
        setHeaderToggle(false);
    } else {
        setHeaderToggle(true);
    }
  }

  return (
    <ThemeProvider theme={navTheme}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            color="inherit"
            aria-label="menu"
            onClick={updateHeader}
            sx={{ mr: 2, borderRadius: '19px' }}
          >
            <TitleIcon />
            { headerToggle ? 
            <ArrowBackIcon /> : <ArrowForwardIcon />
            }
          </IconButton>
          { headerToggle ? 
          <Typography variant="h6" component="div" sx={{ flexGrow: 1, displayPrint: {headerToggle} }}>
            Лабораторна робота №1
          </Typography>
          : null }
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
}