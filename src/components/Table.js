import { useMemo } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


function calculateAvgMark(marks) {
    const sum = Object.values(marks).reduce(function(total, mark) {
      return (total + mark)
    });
  
    const middleMark = sum / Object.values(marks).length;
  
    return middleMark
}

export function CreateTable() {
    const marks = useMemo(() => ({
        'Математика': 4,
        'Історія України': 4,
        'Англійська мова': 5,
        'Українська мова': 4,
        'Філософія': 5,
        "Фізика": 4,
        'Фізра': 4,
        "Комп. логіка": 5,
        'БЖД': 3
      }), []);
    const avgMark = useMemo(() => calculateAvgMark(marks), [marks]);
    
    
    return (
        <TableContainer sx={{ maxWidth: 450 }} component={Paper} elevation={3}>
            <Table aria-label="simple table">
            <TableHead>
                <TableRow>
                    <TableCell sx={{ fontWeight: 'bold', fontSize: '1.5vw' }} align='center'>Предмет</TableCell>
                    <TableCell sx={{ fontWeight: 'bold', fontSize: '1.5vw' }} align='center'>Оцінка</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {Object.keys(marks).map((mark) => (
                <TableRow key={mark}>
                    <TableCell sx={{ fontSize: '1vw' }} align='center' component="th" scope="row">
                    {mark}
                    </TableCell>
                    <TableCell sx={{ fontSize: '1vw' }} align="center">{marks[mark]}</TableCell>
                </TableRow>
                ))}

                <TableRow>
                <TableCell align="center" sx={{ fontWeight: 'bold' }}>Середній бал</TableCell>
                <TableCell align="center" sx={{ fontWeight: 'bold' }}>{avgMark}</TableCell>
                </TableRow>
            </TableBody>
            </Table>
      </TableContainer>
    )
}