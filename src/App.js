import './App.css';
import { CreateTable } from './components/Table';
import CreateNavBar from './components/NavBar';
import { FactorialForm } from './components/FactorialForm';
import Typography from '@mui/material/Typography';


function App() {
  return (
    <>
    <CreateNavBar color="green" />
    <div className="Section">
      <Typography variant='h2' color="white">Оцінки</Typography>
      <CreateTable />
    </div>

    <div className='Section'>
      <Typography variant='h2' color="white">Знайти факторіал</Typography>
      <FactorialForm />
    </div>
    </>
  )
}

export default App;
